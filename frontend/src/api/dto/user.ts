import IAddress from "./address";

export default interface IUser {
	fullname: string;
	email: string;
    address: IAddress;
}