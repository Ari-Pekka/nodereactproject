import IAddress from "./address";
import IUser from "./user";

export interface IUserDto {
	user: IUser
    address: IAddress;
}

export default IUserDto;