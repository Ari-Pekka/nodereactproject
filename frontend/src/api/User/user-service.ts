import IUser from '../dto/user';
import IUserDto from '../dto/userDto';

const API_URL = process.env['REACT_APP_API_URL'] ?? '';

const get = async (url: string): Promise<IUser> => {
  try {
    return await fetch(`${API_URL}${url}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        return data as IUser;
      });
  } catch (error) {
    console.error(error);
  }
  return {} as IUser;
};

const getAll = async (url: string): Promise<IUser[]> => {
  try {
    return await fetch(`${API_URL}${url}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        return data as IUser[];
      });
  } catch (error) {
    console.error(error);
  }
  return [] as IUser[];
};

const post = async (url: string, body: IUserDto): Promise<IUserDto> => {
  try {
    return await fetch(`${API_URL}${url}`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json;charset=UTF-8',
      },
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((data) => {
        return data as IUserDto;
      });
  } catch (error) {
    console.error(error);
  }
  return {} as IUserDto;
};

const userService = {
  getAll,
  get,
  post,
};

export default userService;
