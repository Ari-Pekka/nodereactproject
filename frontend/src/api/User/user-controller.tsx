import IUser from "../dto/user";
import IUserDto from "../dto/userDto"
import userService from "./user-service";

const getAllUsers = async () => {
  try {
    return await userService.getAll("/get-all");
  } catch (error) {
    console.error(error);
    return [] as IUser[];
  }
};

const getUser = async (email: string) => {
  try {
    return await userService.get(`/get-user/${email}`);
  } catch (error) {
    console.error(error);
    return null;
  }
};

const createUser = async (data: IUserDto) => {
  try {
    return await userService.post("/save-user", data);
  } catch (error) {
    console.error(error);
    return null;
  }
};

const userController = {
  getAllUsers,
  getUser,
  createUser
};

export default userController;