import { Route, Switch, Redirect } from 'react-router-dom';
import HomePage from '../pages/Home/home';
import UserPage from '../pages/User/user';

function AppRoutes() {
    return (
        <Switch>
            <Route path="/all" exact={true} component={HomePage} />
            <Route path="/:email" component={UserPage} />
            <Route path="/" exact={true}>
                <Redirect to="/all" />
            </Route>
        </Switch>
    )
}

export default AppRoutes;