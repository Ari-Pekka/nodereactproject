import './Button.css';
import { MouseEventHandler } from "react";

type ButtonProps = {
  onClick: MouseEventHandler<HTMLButtonElement>,
  type: "button" | "submit" | "reset",
  children: string,
  hidden?: boolean
};

function Button(props: ButtonProps) {
  return (
    <div className="buttonArea" hidden={props.hidden ? props.hidden : false}>
      <button className="button" type={props.type} onClick={props.onClick}>
        {props.children}
      </button>
    </div>
  );
}

export default Button;
