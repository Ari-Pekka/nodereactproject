import { ChangeEventHandler } from 'react';
import './Input.css';

type InputProps = {
    id: string,
    type: 'text' | 'email',
    onChange: ChangeEventHandler<HTMLInputElement>,
    label?: string,
};

function Input(props: InputProps) {
    return (
        <div className="inputContainer">
            {props.label ? (
                <div className="labelArea">
                    <label htmlFor={props.id}>{props.label} </label>
                </div>
            ) : null}
            <div className="inputArea">
                <input
                    id={props.id}
                    type={props.type}
                    onChange={props.onChange}
                />
            </div>
        </div>
    );
}


export default Input;
