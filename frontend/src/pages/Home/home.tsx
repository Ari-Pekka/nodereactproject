import { FC, useEffect, useState } from 'react';
import Button from '../../components/Button/Button';
import './home.css';
import userController from '../../api/User/user-controller';
import IUser from '../../api/dto/user';
import { Link } from 'react-router-dom';
import UserForm from '../User/NewUser/userForm';

const HomePage: FC = () => {
    const [showForm, setshowForm] = useState(false);
    const [userList, setUserList] = useState(Array<IUser>);

    useEffect(() => {
        const fetchData = async () => {
            const data: IUser[] | null = await userController.getAllUsers();
            console.log(data)
            setUserList(data);
        }
        if (userList.length === 0) {
            fetchData();
        }
    }, []);

    async function updateUserList() {
        const data: IUser[] = await userController.getAllUsers();
        setUserList(data);
        setshowForm(false);
    }

    if (userList.length === 0) return <>No Users</>
    else {
        return (
            <div className='home'>
                {
                    !showForm ?
                        <div>
                            <div className='header-area'>
                                <h1>Users</h1>
                            </div>
                            <div className="user-area">
                                <ul>
                                    {
                                        userList.map((user: IUser) => {
                                            return (
                                                <li key={user?.email}>
                                                    <Link to={user?.email}>{user?.fullname}</Link>
                                                </li>
                                            )
                                        })
                                    }
                                </ul>
                            </div>
                            <div className='button-area'>
                                <Button onClick={() => setshowForm(true)} type='button'>Save new user</Button>
                            </div>
                        </div>
                        :
                        <div className='form-area'>
                            <div className='form-container'>
                                <h2>New user info</h2>
                                <UserForm updateUserList={updateUserList} />
                                <div className='button-area'>
                                    <Button onClick={() => setshowForm(false)} type='button' hidden={!showForm}>Close form</Button>
                                </div>
                            </div>
                        </div>
                }
            </div>
        )
    }
}

export default HomePage;
