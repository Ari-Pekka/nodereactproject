import { FC, useEffect, useState } from 'react';
import './user.css';
import userController from '../../api/User/user-controller';
import IUser from '../../api/dto/user';

const UserPage: FC = () => {
  const [user, setUser] = useState({} as IUser);

  useEffect(() => {
    const fetchData = async () => {
      const path: string | undefined = window.location.href.split("/").pop();
      if (path !== undefined) {
        const data: IUser | null = await userController.getUser(path);
        if (data !== null) {
          console.log("data", data)
          setUser(data);
        }
      }
    }
    fetchData();
  }, []);

  if (Object.keys(user).length === 0) return <>No user</>
  else {
    return (
      <div className='user-container'>
        <div className='user-window'>
          <div className='header-area'>
            <h1>{user?.fullname}</h1>
            <p>{user?.email}</p>
          </div>
          <div className='address-container'>
            <h2>Address:</h2>
            <p>{user?.address?.name} {user?.address?.street} {user?.address?.postalCode}<br /> {user?.address?.city} <br /> {user?.address?.country}</p>
          </div>
        </div>
      </div>
    )
  }
}

export default UserPage;

