import React, { useState } from 'react';
import './userForm.css';
import Input from '../../../components/Input/Input';
import Button from '../../../components/Button/Button';
import userController from '../../../api/User/user-controller';
import IUserDto from '../../../api/dto/userDto';
import IUser from '../../../api/dto/user';
import IAddress from '../../../api/dto/address';

type UserFormProps = {
    updateUserList: () => void
}

function UserForm({ updateUserList }: UserFormProps) {
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');

    const [name, setName] = useState('');
    const [street, setStreet] = useState('');
    const [city, setCity] = useState('');
    const [country, setCountry] = useState('');
    const [postalCode, setPostalCode] = useState('');

    async function handleCreateUser(e: React.SyntheticEvent) {
        e.preventDefault();

        const Address: IAddress = {
            name: name,
            street: street,
            city: city,
            country: country,
            postalCode: postalCode
        }
        const User: IUser = {
            fullname: fullName,
            email: email,
            address: Address
        }
        const payload: IUserDto = {
            user: User,
            address: Address
        }
        const res = await userController.createUser(payload);
        if (res !== null) {
            alert("success");
            updateUserList();
        }
        else {
            alert("failed");
        }
    }

    return (
        <form onSubmit={handleCreateUser}>
            <div className='user-form'>
                <Input
                    id="fullName"
                    label="Full Name"
                    type='text'
                    key="fullName"
                    onChange={(e) => setFullName(e.target.value)}
                />
                <Input
                    id="email"
                    label="Email"
                    type='email'
                    key="email"
                    onChange={(e) => setEmail(e.target.value)}
                />
            </div>
            <br />
            <h3>Address info</h3>
            <div className='address-form'>
                <Input
                    id="name"
                    label="Street Name"
                    type='text'
                    key="name"
                    onChange={(e) => setName(e.target.value)}
                />
                <Input
                    id="street"
                    label="Street"
                    type='text'
                    key="street"
                    onChange={(e) => setStreet(e.target.value)}
                />
                <Input
                    id="city"
                    label="City"
                    type='text'
                    key="city"
                    onChange={(e) => setCity(e.target.value)}
                />
                <Input
                    id="country"
                    label="Country"
                    type='text'
                    key="country"
                    onChange={(e) => setCountry(e.target.value)}
                />
                <Input
                    id="postal"
                    label="Postal Code"
                    type='text'
                    key="postal"
                    onChange={(e) => setPostalCode(e.target.value)}
                />
            </div>
            <Button type='submit' onClick={(e) => handleCreateUser(e)}>Submit</Button>
        </form>
    )
}

export default UserForm;

