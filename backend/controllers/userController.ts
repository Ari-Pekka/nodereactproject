import { RequestHandler } from 'express';
import userService from '../services/userService';
import IUser from '../interfaces/user';
import IUserDto from '../interfaces/userDto';

export const getUser: RequestHandler = async (req, res, next) => {
  try {
    console.log('get user called');
    const { email } = req.params;
    let user: IUser | null = null;
    if (email === null || email === '' || email === undefined) {
      res.status(400).send('Email missing from req.params');
    } else {
      user = await userService.getUser(email);
      if (user !== null) {
        res.status(200).json(user);
      } else {
        console.error('User not found');
        res.status(404).send('Not found');
      }
    }
  } catch (error) {
    console.error(error);
    res.status(500).send('System error');
  }
};

export const getAllUsers: RequestHandler = async (req, res, next) => {
  try {
    console.log('get all users called');
    const users: IUser[] | null = await userService.getAllUsers();
    if (users !== null && users.length > 0) {
      res.status(200).json(users);
    } else {
      console.error('Users not found');
      res.status(404).send('Not found');
    }
  } catch (error) {
    console.error(error);
    res.status(500).send('System error');
  }
};

export const saveUser: RequestHandler = async (req, res, next) => {
  try {
    console.log('save user called');
    const userToSave: IUserDto = req.body;
    if (userToSave?.address === null) {
      res.status(400).send('request body missing');
    }
    const user: IUser = await userService.saveUser(userToSave);
    res.status(200).json(user);
  } catch (error) {
    console.error(error);
    res.status(500).send('System error');
  }
};
