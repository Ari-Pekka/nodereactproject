import { IUser } from '../interfaces/user';
import mongoose from '../database/database';

export const UserSchema = new mongoose.Schema<IUser>({
    fullname:  { type: String },
	email: { type: String, unique: true },
    address: { type: mongoose.Schema.Types.ObjectId, ref: 'Address' }
}, {
	timestamps: true
});

const User = mongoose.model<IUser>('User', UserSchema);

export default User;