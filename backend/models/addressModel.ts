import  IAddress from '../interfaces/address';
import mongoose from '../database/database';

export interface IAddressModel extends IAddress, mongoose.Document {
	generateAddress(): string;
}

export const AddressSchema = new mongoose.Schema<IAddressModel>({
    name:  { type: String },
	street: { type: String },
    city: { type: String },
    country: { type: String },
    postalCode: { type: String }
}, {
	timestamps: true
});

AddressSchema.methods.getFullAddress = function (address: IAddress): string {
    return address.name + " " + address.street + " " + address.postalCode + " " + address.country;
}

const Address = mongoose.model<IAddressModel>('Address', AddressSchema);

export default Address;
