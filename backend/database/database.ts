import mongoose from 'mongoose';
import { MongoError } from 'mongodb';

export class Database {
	public static init (): any {
		const mongoUrl: string | undefined = process.env.MONGO_URL
        if (mongoUrl === null || mongoUrl === "" || mongoUrl === undefined) {
            throw new Error("Missing configuration process.env.MONGOOSE_URL");
        }

		mongoose.connect(mongoUrl)
			.then(() =>  console.info('connected to mongo server at: ' + mongoUrl))
            .catch((error: MongoError) => {
				console.error('Failed to connect to the Mongo server!!');
				console.log(error);
				throw error;
		});
	}
}

export default mongoose;