import express, { Express, Request, Response, NextFunction } from 'express';
import dotenv from 'dotenv';
import userRoutes from './routes/userRoute';
import { Database } from './database/database';

const app: Express = express();
var cors = require('cors');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
dotenv.config();

const port = process.env.PORT;

app.use('/', userRoutes);
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  res.status(500).json({ message: err.message });
});

app.listen(port, () => {
  try {
    console.log('init db...');
    Database.init();
  } catch (error) {
    console.log('Db connection failed', error);
  }
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
