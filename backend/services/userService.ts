import User from '../models/userModel';
import { IUser } from '../interfaces/user';
import Address from '../models/addressModel';
import IUserDto from '../interfaces/userDto';

const UserService = {
  getAllUsers: async (): Promise<IUser[] | null> => {
    const users: IUser[] = await User.find({});
    return users;
  },
  getUser: async (email: string): Promise<IUser | null> => {
    const user: IUser | null = await User.findOne({ email: email })
      .populate('address')
      .exec();
    return user;
  },
  saveUser: async (userDto: IUserDto): Promise<IUser> => {
    const address = new Address({
      ...userDto.address,
    });
    await address.save();

    const newUser = new User({
      ...userDto.user,
      address: address,
    });
    await newUser.save();

    return newUser;
  },
};

export default UserService;
