import express from 'express';
import { getUser, getAllUsers, saveUser } from '../controllers/userController';

const router = express.Router();

router.get('/get-user/:email', getUser);
router.get('/get-all', getAllUsers);
router.post('/save-user', saveUser);


export default router