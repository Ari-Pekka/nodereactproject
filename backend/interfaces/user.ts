import IAddress from "./address";

export interface IUser {
	fullname: string;
	email: string;
    address: IAddress;
}

export default IUser;