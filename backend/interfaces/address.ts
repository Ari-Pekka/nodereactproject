export default interface IAddress {
	name: string;
	street: string;
	city: string;
	country: string;
	postalCode: string;
}
